<div class="modal-item-detail modal-dialog" role="document" data-latitude="" data-longitude="" data-address="">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2></h2>
                <div class="label label-default"></div><div class="controls-more">
                    <ul>
                        <li><a href="#">Add to favorites</a></li>
                        <li><a href="#">Add to watchlist</a></li>
                    </ul>
                </div>
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left"><div class="map" id="map-modal"></div>
                <!--end map-->

                <section>
                <h3>Contact</h3></section>
                <section>
                    <h3>Social Share</h3>
                    <div class="social-share"></div>
                </section>
            </div>
            <!--end left -->
            <div class="right">
                <section>
                    <h3>About</h3>
                    <p></p>
                    <a href="#" class="btn btn-primary btn-framed btn-light-frame btn-rounded btn-xs">Show More</a>
                </section>
                <!--end about--></div>
            <!--end right-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->
